#include "MessagesSender.h"
#include <chrono>
#include <thread>
#include <fstream>
#include <algorithm>
#include <mutex>
#include <condition_variable>

#define WAIT 10
mutex messages_mutex;
mutex users_mutex;

condition_variable cond;

MessagesSender::MessagesSender()
{

}

void MessagesSender::showMenu()
{
	int choice = 0;
	bool exit = false;

	while (!exit)
	{
		cout << "Welcome! What do you want to do? " << endl << "1. sign in" << endl << "2. sign out" << endl << "3. See who is online" << endl << "4.exit" << endl << "input: ";
		cin >> choice;

		switch (choice)
		{
		case 1:
			signIn();
			break;
		case 2:
			signOut();
			break;
		case 3:
			cout << "The connected users are: " << toString() << endl;
			break;
		case 4:
			exit = true;
			cout << "goodbye!";
			break;
		default:
			cout << "invalid input.";
			break;
		}
		cout << endl << endl;
	}
}
void MessagesSender::signIn()
{
	string uName;
	cout << "Please enter a username: ";
	cin >> uName;
	
	list<string>::iterator it = (list<string>::iterator) find(_connectedUsers.begin(), _connectedUsers.end(), uName);
	
	if (it != _connectedUsers.end())
	{
		cout << "The user is already logged in!" << endl;
		return;
	}
	

	lock_guard<mutex> guard(users_mutex);
	_connectedUsers.push_back(uName);
	cout << "Successfully logged in as " << uName << "!" << endl;

}

void MessagesSender::signOut()
{
	string uName;
	cout << "Please enter your username: ";
	cin >> uName;

	list<string>::iterator it = (list<string>::iterator) find(_connectedUsers.begin(), _connectedUsers.end(), uName);

	if (it != _connectedUsers.end())
	{
		lock_guard<mutex> guard(users_mutex);
		_connectedUsers.erase(it);
		cout << "Signed out successfully!" << endl;
		return;
	}


	cout << "User not found." << endl;

}

string MessagesSender::toString()
{
	list<string>::iterator it;
	string rtn = "";

	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		rtn += *it + " ";
	}

	return rtn;
}


void MessagesSender::readFile()
{
	ifstream data;
	data.open("data.txt");

	if (!data.is_open())
	{
		cout << "Couldn`t find data.txt. Please create the file and restart the program." << endl;
		system("pause");
		exit(0);
	}
	data.close();

	while (true)
	{
		this_thread::sleep_for(std::chrono::seconds(WAIT));
		string line;
		data.open("data.txt");
		while (getline(data, line))
		{
			lock_guard<mutex> guard(messages_mutex);
			_messages.push(line);
		}
		cond.notify_all();
		
		data.close();
		data.open("data.txt", ios::trunc | ios::out | ios::in);
		data.close();
	}
}

void MessagesSender::sendMessages()
{
	while (true)
	{
		unique_lock<mutex> guard(messages_mutex);
		cond.wait(guard, [&](){return !_messages.empty(); });
		while (!_messages.empty())
		{
			string a = _messages.front();
			list<string>::iterator it = _connectedUsers.begin();

			lock_guard<mutex> guard(users_mutex);
			for (; it != _connectedUsers.end(); it++)
			{
				ofstream output("output.txt", ios::app);
				output << *it << ": " << a << endl;
			}


			_messages.pop();
		}
	}
	
}