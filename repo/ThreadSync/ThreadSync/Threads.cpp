#include <iostream>
#include <thread>
#include <vector>
#include <ctime>
#include <mutex>
#include <fstream>

using namespace std;

mutex lck;

void I_Love_Threads()
{
	cout << "I love threads!" << endl;
}

void call_I_Love_Threads()
{

	thread ilovethread(I_Love_Threads);
	ilovethread.detach();
}


bool isPrime(int num)
{
	bool result = true;
	if (num <= 2)
	{
		return false;
	}

	for (int i = 2; i <= sqrt(num); i++)
	{
		if (num % i == 0)
		{
			result = false;
			break;
		}
	}

	return result;
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	for (int i = begin; i <= end; i++)
	{
		if (isPrime(i))
		{
			primes.push_back(i);
		}
	}
}

void printVector(vector<int>& primes)
{
	vector<int>::iterator i = primes.begin();

	for (; i != primes.end(); i++)
	{
		cout << *i << endl;
	}
}


vector<int>* callGetPrimes(int begin, int end)
{
	vector<int>* newVector = new vector<int>();
	clock_t start;
	double duration;

	start = clock();
	thread primesThread(getPrimes, begin, end, ref(*newVector));
	primesThread.join(); //Waiting for primesThread to end
	
	duration = (clock() - start) / (double)CLOCKS_PER_SEC;

	cout << "It took" << duration << " seconds for getPrimes() to finish" << endl;

	return newVector;

	
}


void writePrimesToFile(int begin, int end, ofstream& file)
{
	if (file.is_open())
	{
		for (int i = begin; i < end; i++)
		{
			if (isPrime(i))
			{
				lck.lock();
				file << i << endl;
				lck.unlock();
			}
		}

	}

	else
	{
		cout << "The file is not opened." << endl;
	}
}


void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	int current = begin;
	int difference = (end - begin) / N;
	int currentEnd = 0;
	vector<thread*> threads;
	ofstream file(filePath);
	thread* currThread = NULL;

	clock_t start;
	double duration;

	start = clock();
	for (int i = 0; i < N; i++)
	{
		currentEnd = current + difference;
		if (currentEnd > end)
		{
			currentEnd = end;
		}

		
		currThread = new thread(writePrimesToFile, current, currentEnd, ref(file));
		threads.push_back(currThread);


		current += difference + 1;
	}

	
	for (int i = 0; i < N; i++)
	{
		currThread = threads.back();
		currThread->join();
		delete currThread;
		threads.pop_back();
	}

	duration = (clock() - start) / (double)CLOCKS_PER_SEC;

	cout << "It took" << duration << " seconds for callWritePrimesMultipleThreads to finish" << endl;

	
}