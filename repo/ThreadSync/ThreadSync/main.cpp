#include <iostream>
#include "Threads.h"
#include "MessagesSender.h"
#include <thread>

using namespace std;

void main()
{
	MessagesSender ms;
	thread data(&MessagesSender::readFile, &ms);
	thread messages(&MessagesSender::sendMessages, &ms);
	data.detach();
	messages.detach();
	ms.showMenu();
	system("pause");
}
