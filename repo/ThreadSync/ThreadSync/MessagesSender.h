#pragma once

#include <iostream>
#include <string>
#include <queue>
#include <list>

using namespace std;

class MessagesSender
{
private:
	list<string> _connectedUsers;
	queue<string> _messages;
	
public:
	MessagesSender();
	void signIn(); //Scans a username and signs in
	void signOut(); //Scans a username and signs out
	string toString(); //Returns the connected users in a string format
	void showMenu(); // Menu
	void readFile(); //Reads the data file and retrives messages.
	void sendMessages(); //Sends the messages to every online user.
	
};

